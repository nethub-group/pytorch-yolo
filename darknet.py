import torch
import torch.nn as nn

import numpy as np

class EmptyLayer(nn.Module):
    def __init__(self):
        super(EmptyLayer, self).__init__()


class DetectionLayer(nn.Module):
    def __init__(self, anchors):
        super(DetectionLayer, self).__init__()
        self.anchors = anchors

    def forward(self, x, inp_dim, num_classes, confidence):
        x = x.data
        global CUDA
        prediction = x
        prediction = predict_transform(
            prediction,
            inp_dim,
            self.anchors,
            num_classes,
            confidence,
            CUDA,
        )

        return prediction


class Darknet(nn.Module):
    def __init__(self, cfgfile):
        super(Darknet, self).__init__()
        self.blocks = self.parse_cfg(cfgfile)
        self.net_info, self.module_list = self.create_modules(self.blocks)
        self.header = torch.IntTensor([0,0,0,0])
        self.seen = 0

    def get_blocks(self):
        return self.blocks

    def get_module_list(self):
        return self.module_list

    def forward(self, x, CUDA):
        detections = []
        modules = self.blocks[1:]
        # We cache the outputs for the route layer
        write = 0

    def parse_cfg(self, cfgfile):
        """
        Takes a configuration file

        Returns a list of blocks. Each block describes a block in the neural
        network to be built. Block is represented as a dictionary in the list
        """

        file = open(cfgfile, 'r')
        # Store the lines in a list
        lines = file.read().split('\n')
        # Get read of empty lines
        lines = [x for x in lines if len(x) > 0]
        lines = [x for x in lines if x[0] != '#']
        lines = [x.rstrip().lstrip() for x in lines]

        block = {}
        blocks = []

        # This marks the start of a new block
        for line in lines:
            if line[0] == "[":
                if len(block) != 0:
                    blocks.append(block)
                    block = {}

                block["type"] = line[1:-1].rstrip()
            else:
                key, value = line.split("=")
                block[key.rstrip()] = value.lstrip()

        blocks.append(block)

        return blocks

    def create_modules(self, blocks):
        # Captures the information about the input and pre-processing
        net_info = blocks[0]

        module_list = nn.ModuleList()

        # Indexing blocks helps with implementing route layers (skip connections)
        index = 0

        prev_filters = 3
        output_filters = []

        for x in blocks:
            module = nn.Sequential()

            if (x["type"] == "net"):
                continue

            # If it's a convolutional layer
            if (x["type"] == "convolutional"):
                # Get the info about the layer
                activation = x["activation"]

                try:
                    batch_normalize = int(x["batch_normalize"])
                    bias = False
                except:
                    batch_normalize = 0
                    bias = True

                filters = int(x["filters"])
                padding = int(x["pad"])
                kernel_size = int(x["size"])
                stride = int(x["stride"])

                if padding:
                    pad = (kernel_size - 1) // 2
                else:
                    pad = 0

                # Add the convolutional layer
                conv = nn.Conv2d(
                    prev_filters,
                    filters,
                    kernel_size,
                    stride,
                    pad,
                    bias = bias,
                )
                module.add_module("conv_{0}".format(index), conv)

                # Add the Batch Norm Layer
                if batch_normalize:
                    bn = nn.BatchNorm2d(filters)
                    module.add_module("batch_norm_{0}".format(index), bn)

                # Check the activation
                # It is either Linear or Leaky ReLU for YOLO
                if activation == "leaky":
                    activn = nn.LeakyReLU(0.1, inplace = True)
                    module.add_module("leaky_{0}".format(index), activn)

            elif (x["type"] == "upsample"):
                stride = int(x["stride"])
                upsample = nn.Upsample(
                    scale_factor = 2,
                    mode = "nearest",
                )
                module.add_module("upsample_{}".format(index), upsample)

            elif (x["type"] == "route"):
                x["layers"] = x ["layers"].split(',')

                # Start of a route
                start = int(x["layers"][0])

                # End, if there exists one.
                try:
                    end = int(x["layers"][1])
                except:
                    end = 0

                # Positive anotation
                if start > 0:
                    start = start - index

                if end > 0:
                    end = end - index

                route = EmptyLayer()
                module.add_module("route_{0}".format(index), route)


                if end < 0:
                    filters = output_filters[index + start] + \
                                output_filters[index + end]
                else:
                    filters = output_filters[index + start]


            # Shortcut corresponds to skip connection
            elif x["type"] == "shortcut":
                from_ = int(x["from"])
                shortcut = EmptyLayer()
                module.add_module("shortcut_{}".format(index), shortcut)

            elif x["type"] == "maxpool":
                stride = int(x["stride"])
                size = int(x["size"])
                if stride != 1:
                    maxpool = nn.MapPool2d(size, stride)
                else:
                    maxpool = MaxPoolStride1(size)

                module.add_module("maxpool_{}".format(index), maxpool)

            # YOLO is the detection layer
            elif x["type"] == "yolo":
                mask = x["mask"].split(",")
                mask = [int(x) for x in mask]

                anchors = x["anchors"].split(",")
                anchors = [int(a) for a in anchors]
                anchors = [
                    (anchors[i], anchors[i+1]) for i in range(0, len(anchors), 2)
                ]

                detection = DetectionLayer(anchors)
                module.add_module("Detection_{}".format(index), detection)

            else:
                print("Something I dunno")
                assert False

            module_list.append(module)
            prev_filters = filters
            output_filters.append(filters)
            index += 1

        return (net_info, module_list)


    def load_weights(self, weightfile):

        # Open the weights file
        fp = open(weightfile, "rb")

        # The first 4 values are header information
        # 1. Major version number
        # 2. Minor version number
        # 3. Subversions number
        # 4. Images seen
        header = np.fromfile(
            fp,
            dtype=np.int32,
            count=5,
        )

        # The rest of the values are the weights
        weights = np.fromfile(fp, dtype=np.float32)

        ptr = 0

        for i in range(len(self.module_list)):
            module_type = self.blocks[i+1]["type"]

            if module_type == "convolutional":
                model = self.module_list[i]

                try:
                    batch_normalize = int(self.blocks[i+1]["batch_normalize"])
                except:
                    batch_normalize = 0

                conv = model[0]

                if (batch_normalize):
                    bn = model[1]

                    # Get the number of weights of Batch Norm Layer
                    num_bn_biases = bn.bias.numel()

                    # Load the weights
                    bn_biases = torch.from_numpy(weights[ptr:ptr + num_bn_biases])
                    ptr += num_bn_biases

                    bn_weights = torch.from_numpy(weights[ptr:ptr + num_bn_biases])
                    ptr += num_bn_biases

                    bn_running_mean = torch.from_numpy(weights[ptr:ptr + num_bn_biases])
                    ptr += num_bn_biases

                    bn_running_var = torch.from_numpy(weights[ptr: ptr + num_bn_biases])
                    ptr += num_bn_biases

                    # Cast the loaded weights into dims of model weights
                    bn_biases = bn_biases.view_as(bn.bias.data)
                    bn_weights = bn_weights.view_as(bn.weight.data)
                    bn_running_mean = bn_running_mean.view_as(bn.running_mean)
                    bn_running_var = bn_running_var.view_as(bn.running_var)

                    # Copy the data to model
                    bn.bias.data.copy_(bn_biases)
                    bn.weight.data.copy_(bn_weights)
                    bn.running_mean.copy_(bn_running_mean)
                    bn.running_var.copy_(bn_running_var)

                else:
                    # Number of biases
                    num_biases = conv.bias.numel()

                    # Load the weights
                    conv_biases = torch.from_numpy(weights[ptr: ptr + num_biases])
                    ptr = ptr + num_biases

                    # Reshape the loaded weights according to the dims of the model weights
                    conv_biases = conv_biases.view_as(conv.bias.data)

                    # Finally copy the data
                    conv.bias.data.copy_(conv_biases)

                # Let us load the weights for the convolutional layers
                num_weights = conv.weight.numel()

                # Do the same as above for weights
                conv_weights = torch.from_numpy(weights[ptr:ptr+num_weights])
                ptr = ptr + num_weights

                conv_weights = conv_weights.view_as(conv.weight.data)
                conv.weight.data.copy_(conv_weights)
