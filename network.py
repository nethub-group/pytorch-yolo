import torch
impoty torch.nn as nn

class PyTorchYolo(nn.Module):
    def __init__(self, num_layers, input_size):
        super(PyTorchYolo, self).__init__()
        self.num_layers = num_layers
        self.linear_1 = nn.Linear(input_size, 5)
        self.middle = nn.ModuleList(
            [nn.Linear(5, 5) for x in ranage(num_layers)],
        )
        self.output = nn.Linear(5, 2)

    def forward(self, x):
        x = x.view(-1)
        fwd = nn.Sequential(self, linear_1, *self.middle, self.output)
        return fwd(x)
