''' Settings '''
import torch


def load_classes(namesfile):
    fp = open(namesfile, "r")
    names = fp.read().split("\n")[:-1]
    return names


images_path = 'data_set/test_img/'
# Batch Size
batch_size = 1
# Object Confidence to filter predictions
confidence = 0.5
# NMS Threshhold
nms_thresh = 0.4
# Scales to use for detection
scales = [1,2,3]

start = 0

CUDA = torch.cuda.is_available()

num_classes = 80
classes = load_classes('data_set/coco.names')

# Input resolution of the network. Increase to
# increase accuracy. Decrease to increase speed.
reso = 416
