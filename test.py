import os
import os.path as osp

import torch
from torch.autograd import Variable

from darknet import Darknet
from preprocessor import (
    prep_image,
    write_results,
)

# Settings
from settings import (
    CUDA,
    reso,
    images_path,
    batch_size,
    confidence,
    num_classes,
    nms_thresh,
)


leftover = 0

model = Darknet('net_data/yolov3.cfg')
model.load_weights('net_data/yolov3.weights')
model.net_info["height"] = reso

inp_dim = int(model.net_info["height"])

assert inp_dim % 32 == 0
assert inp_dim > 32

# If there's a GPU available, put the model on GPU
if CUDA:
    model.cuda()

# Set the model in evaluation mode
model.eval()

#Detection phase
try:
    imlist = [osp.join(osp.realpath('.'), images_path, img) for img in os.listdir(images_path)]
except NotADirectoryError:
    imlist = []
    imlist.append(osp.join(osp.realpath('.'), images_path))
except FileNotFoundError:
    print('No file or directory with the name {}'.format(images_path))
    exit()

if not os.path.exists('det/'):
    os.makedirs('det/')

batches = list(map(prep_image, imlist, [inp_dim for x in range(len(imlist))]))
im_batches = [x[0] for x in batches]
orig_ims = [x[1] for x in batches]
im_dim_list = [x[2] for x in batches]
im_dim_list = torch.FloatTensor(im_dim_list).repeat(1,2)

if CUDA:
    im_dim_list = im_dim_list.cuda()

if (len(im_dim_list) % batch_size):
    leftover = 1

if batch_size != 1:
    num_butches = len(imlist) // batch_size + leftover
    im_batches = [
        torch.cat(
            (
                im_batches[i*batch_size : min(
                    (i + 1)*batch_size,
                    len(im_batches)
                )]
            )
        ) for i in range(num_butches)
    ]

i = 0
write = False
objs = {}

for batch in im_batches:
    if CUDA:
        batch = batch.cuda()

    """
    Apply offsets to the result predictions.
    Transform the predictions as described in the YOLO paper.
    Flatten the prediction vector.
    """

    with torch.no_grad():
        prediction = model(Variable(batch), CUDA)

    print(prediction)

    prediction = write_results(
        prediction,
        confidence,
        num_classes,
        nms = True,
        nms_conf = nms_thresh,
    )

    if type(prediction) == int:
        i += 1
        continue

    prediction[:, 0] += i*batch_size

    if not write:
        output = prediction
        write = 1
    else:
        output = torch.cat((output, prediction))

    for im_num, image in enumerate(imlist[
        i*batch_size : min(
            (i+1)*batch_size,
            len(imlist)
        )
    ]):
        im_id = i*batch_size + im_num
        objs = [classes[int(x[-1])] for x in output if int(x[0]) == im_id]
        print("Objects Detected %s" % (objs))

    i+=1

    if CUDA:
        torch.cuda.synchronize()

try:
    output
except NameError:
    print("No detections")
    exit()
